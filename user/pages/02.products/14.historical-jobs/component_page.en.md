---
title: 'Historical jobs'
custom:
    title: 'Historical jobs'
    description: 'All job ads that have been published by Arbetsförmedlingen since 2006.'
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
    menu:
        -
            title: Explore
            url: 'http://historik.azurewebsites.net'
            showInShort: '1'
        -
            title: 'Getting started'
            url: 'https://github.com/Jobtechdev-content/HistoricalJobs-content/blob/develop/GettingStartedHistoricalJobsSE.md'
            showInShort: '1'
    block_1: "Historical Jobs is a dataset with all job ads that have been published on Platsbanken since 2006. It also contains metadata (information about service, employer, place, time, etc.) and free text information. The dataset is continuously updated.\n \n## For whom is the product created?\nHistorical Jobs is useful for all companies and organizations that need large amounts of advertising data for statistics and analyzes in the labor market area. The large data base can also be used to build and train algorithms in connection with machine learning and for the development of new digital matching services."
    block_2: "Naming attributes for ad objects follows the naming used in JobStream. Recent years' ad objects contain more attributes than before. Values for attributes that do not exist are null.\n\n### Download\nAll: [2006-2021](https://minio.arbetsformedlingen.se/historiska-annonser/zip/pb_2006_2021.zip)\n\nYear by year: [2006](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2006.zip), [2007](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2007.zip), [2008](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2008.zip), [2009](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2009.zip), [2010](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2010.zip), [2011](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2011.zip), [2012](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2012.zip), [2013](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2013.zip), [2014](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2014.zip), [2015](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2015.zip), [2016](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2016.zip), [2017](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2017.zip), [2018](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2018.zip), [2019](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2019.zip), [2020](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2020.zip), [2021](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2021.zip)"
    product_info:
        -
            title: 'Data format'
            value: JSON
taxonomy:
    category:
        - Dataset
    type:
        - 'Open data'
---

