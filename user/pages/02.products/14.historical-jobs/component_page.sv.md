---
title: 'Historical jobs'
custom:
    title: 'Historiska jobb'
    description: 'Alla jobbannonser som har publicerats hos Arbetsförmedlingen sedan 2006.'
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
    menu:
        -
            title: Utforska
            url: 'http://historik.azurewebsites.net'
            showInShort: '1'
        -
            title: 'Kom igång'
            url: 'https://github.com/Jobtechdev-content/HistoricalJobs-content/blob/develop/GettingStartedHistoricalJobsSE.md'
            showInShort: '1'
    block_1: "Historiska jobb är ett dataset med alla jobbannonser som har publicerats på Platsbanken sedan 2006. Det innehåller även metadata (information om tjänst, arbetsgivare, ort, tidpunkt osv) och fritextinformation. Datasetet uppdateras kontinuerligt.\n \n## Vem kan ha nytta av produkten?\nHistoriska jobb är användbart för alla företag och organisationer som behöver stora mängder annonsdata för statistik och analyser inom arbetsmarknadsområdet. Det stora dataunderlaget kan även användas för att bygga och träna algoritmer i samband med maskininlärning samt för utveckling av nya digitala matchningstjänster."
    block_2: "Namngivning av attribut för annonsobjekt följer namngivningen som används i JobStream. Senare års annonsobjekt innehåller fler attribut än tidigare. Värden för attribut som inte finns är null.\n\n### Ladda ner\nAlla: [2006-2021](https://minio.arbetsformedlingen.se/historiska-annonser/zip/pb_2006_2021.zip)\n\nÅr för år: [2006](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2006.zip), [2007](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2007.zip), [2008](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2008.zip), [2009](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2009.zip), [2010](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2010.zip), [2011](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2011.zip), [2012](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2012.zip), [2013](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2013.zip), [2014](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2014.zip), [2015](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2015.zip), [2016](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2016.zip), [2017](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2017.zip), [2018](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2018.zip), [2019](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2019.zip), [2020](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2020.zip), [2021](https://minio.arbetsformedlingen.se/historiska-annonser/zip/2021.zip)"
    product_info:
        -
            title: Dataformat
            value: JSON
taxonomy:
    category:
        - Dataset
    type:
        - 'Öppna data'
---

