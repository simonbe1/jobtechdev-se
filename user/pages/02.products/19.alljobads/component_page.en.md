---
title: AllJobAds
custom:
    title: AllJobAds
    description: 'A widget that shows selected jobs from the Arbetsförmedlingen platform Platsbanken.'
    block_1: "AllJobAds is a plug-and-play widget that displays jobs from the Arbetsförmedlingen platform Platsbanken. The widget can be set for defined areas and it is possible to filter results.\n\n## What problem does the product solve?\nMany companies and organizations want to have a simple list of jobs available on their website. The widget is designed to be easily changed and tailored based on specific wishes and create added value for the website's visitors.\n\n## For whom is the product created?\nAllJobAds is mainly intended for companies and organizations that want to guide people to jobs in a specific area, e.g. municipality, company or profession."
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
    menu:
        -
            title: Demo
            url: 'https://widgets.jobtechdev.se/alljobads/'
            showInShort: '1'
        -
            title: 'Demo (without modal)'
            url: 'https://widgets.jobtechdev.se/alljobads/notModal.html'
            showInShort: '1'
        -
            title: 'Source code'
            url: 'https://github.com/MagnumOpuses/allJobAdsWidget'
            showInShort: '1'
        -
            title: README
            url: 'https://github.com/MagnumOpuses/allJobAdsWidget/blob/master/README.md'
            showInShort: null
taxonomy:
    category:
        - Widget
    type:
        - 'Open source'
---

