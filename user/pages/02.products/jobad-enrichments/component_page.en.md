---
title: 'JobAd Enrichments'
custom:
    description: 'AI solution that identifies and extracts relevant information in unstructured job advertisements.'
    contact_name: 'Josefin Berndtson'
    version: '1.0'
    format: JSON
    licens: MIT
    publishdate: '19-05-2021 10:00'
    block_1: "JobAd Enrichments is an AI solution that automatically retrieves relevant words and phrases in job ads while filtering out redundant information. The API contributes to a more accurate match between employers and jobseekers, and makes it easier to navigate and quickly find your way on digital advertising platforms.\n\n## What problem does the product solve?\nIrrelevant search results are a common problem among users of digital matching services and advertising platforms. Being forced to spend time cleaning and sorting among search results makes job search more difficult. With JobAd Enrichments, problems can be minimized, and players offering digital services do not have to spend time and resources on manual management.\n\n## For whom is the product created?\nJobAd Enrichments is useful for all companies and organizations that offer a digital matching service or advertising platform, and who want to improve it. The API can also be used to develop new innovative digital services or to gain in-depth insight into the labor market."
    block_2: "### Implementation and use\nWe recommend that you contact us before implementing JobAd Enrichments directly to your system so that we can provide advice and guidance on the API's capabilities and limitations."
    contact_email: josefin.berndtson@arbetsformedlingen.se
    title: 'JobAd Enrichments'
    menu:
        -
            title: 'User Interface'
            url: 'https://jobad-enrichments-api.jobtechdev.se'
            showInShort: '1'
        -
            title: 'API key'
            url: 'https://apirequest.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Getting started'
            url: 'https://github.com/Jobtechdev-content/JobAdEnrichments-content/blob/master/GettingstartedJobAdEnrichmentsSE.md'
            showInShort: '1'
    product_info:
        -
            title: Version
            value: '1.0'
        -
            title: 'Data type'
            value: JSON
taxonomy:
    category:
        - API
    type:
        - 'Open data'
---

