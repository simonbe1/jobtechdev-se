---
title: 'Jobtech Taxonomy'
custom:
    title: 'Jobtech Taxonomy'
    description: 'Yrkesbenämningar, kompetensbegrepp och kopplingen mellan yrken och kompetenser.'
    block_1: "JobTech Taxonomy ger tillgång till de ord och begrepp som används på arbetsmarknaden samt information om hur dessa är kopplade till varandra. Det kan till exempel vara yrkesbenämningar, kompetensbegrepp eller kopplingen mellan yrken och kompetenser. Yrkesbenämningarna är strukturerade enligt Standard för Svensk Yrkesklassificering (SSYK).\n \n## Vilket problem löser produkten?\nOtydligt eller felaktigt formulerade jobbannonser och CV:n är ett problem som kan leda till att arbetsgivare och arbetssökande missar varandra. Med API:et skapas ett gemensamt språk för arbetsmarknaden, som gör det lättare för arbetsgivare och arbetssökande att förstå och hitta varandra i digitala kanaler.\n \n## Vem kan ha nytta av produkten?\nJobTech Taxonomy är användbart för alla företag och organisationer som erbjuder digitala matchningstjänster men som inte har möjlighet att bygga upp en informationsstruktur på egen hand. Aktörer som studerar och analyserar utvecklingen på arbetsmarknaden har också nytta av API:et.\n"
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berntson'
    menu:
        -
            title: Användargränssnitt
            url: 'https://taxonomy.api.jobtechdev.se/v1/taxonomy/swagger-ui/index.html'
            showInShort: '1'
        -
            title: 'Kom igång'
            url: 'https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/develop/GETTINGSTARTED.md'
            showInShort: '1'
        -
            title: Community
            url: 'https://forum.jobtechdev.se/c/jobtech-taxonomy/11'
            showInShort: null
        -
            title: Källkod
            url: 'https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/develop/GETTINGSTARTED.md'
            showInShort: '1'
    block_2: "### Notera\nInnehållet i taxonomierna uppdateras ständigt och ändringar släpps på ett kontrollerat sätt.<BR>\n"
    product_info:
        -
            title: Version
            value: '1.0 (2.0 beta)'
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
        - 'Öppen källkod'
---

