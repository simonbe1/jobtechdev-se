---
title: 'Jobtech Taxonomy'
custom:
    title: 'Jobtech Taxonomy'
    description: 'Job titles, competence concepts and the connection between professions and skills.'
    block_1: "JobTech Taxonomy provides access to the words and concepts used in the labor market as well as information on how these are linked to each other. It can be, for example, job titles, competence concepts or the connection between professions and skills. The occupational names are structured according to the Standard for Swedish Occupational Classification (SSYK).\n \n## What problem does the product solve?\nUnclear or incorrectly worded job advertisements and the CV are a problem that can lead to employers and jobseekers missing each other. The API creates a common language for the labor market, which makes it easier for employers and jobseekers to understand and find each other in digital channels.\n \n## For whom is the product created?\nJobTech Taxonomy is useful for all companies and organizations that offer digital matching services but do not have the opportunity to build an information structure on their own. Actors who study and analyze progress in the labor market also benefit from the API.\n"
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berntson'
    menu:
        -
            title: 'User Interface'
            url: 'https://taxonomy.api.jobtechdev.se/v1/taxonomy/swagger-ui/index.html'
            showInShort: '1'
        -
            title: 'Getting started'
            url: 'https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/develop/GETTINGSTARTED.md'
            showInShort: '1'
        -
            title: Community
            url: 'https://forum.jobtechdev.se/c/jobtech-taxonomy/11'
            showInShort: null
        -
            title: 'Open source code'
            url: 'https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api/-/blob/develop/GETTINGSTARTED.md'
            showInShort: '1'
    block_2: "### Please note\nThe content of the taxonomies is constantly updated and changes are released in a controlled manner.<BR>\n"
    product_info:
        -
            title: Version
            value: '1.0 (2.0 beta)'
taxonomy:
    category:
        - API
    type:
        - 'Open data'
        - 'Open source'
---

