---
title: 'Giglab Sverige'
custom:
    title: 'Giglab Sverige'
    description: 'A national policy lab and forum with the aim of highlighting challenges from several perspectives in the gig economy and the labor market of the future.'
    block_1: "Giglab Sweden is a national policy lab and a collaboration between Jobtech Development / Arbetsförmedlingen, Skatteverket, Coompanion and Handelshögskolan together with SVID, Stiftelsen Svensk Industridesign and Swedish Labbnätverket. Part of the lab is part of the Vinnova call Challenge-driven Innovation. Our ambition is to work together for a sustainable growth of the gig economy in Sweden.\n\n## Objective\n- Basis for a test bed for new prototype sustainable solutions in the gig economy.\n- Basis for the policy for a continued discussion about the need for policy changes and changes in the law linked to the gig economy.\n- Input in ongoing work towards Agenda 2030, focus on better working conditions and digital infrastructure linked to the gig economy.\n- Basis for continued internal work at the authorities linked to the gig economy\n- Empirical basis for research in the field of gig economics, which i.a. conducted by Postdok researcher Claire Ingram Bogusz at the Stockholm School of Economics\n\n## Method\nGiglab Sverige is a first joint learning project to investigate how we can contribute to creating a sustainable future labor market that includes sustainable gigging, which creates value for society, the individual and for different business models in a digital and global world."
    menu:
        -
            title: Website
            url: 'https://www.giglabsverige.se'
            showInShort: '1'
        -
            title: Report
            url: 'https://assets.website-files.com/5e95f95bba7c1422fb23179c/60801ba984287bfc458ad010_GiglabSverige-Kartläggning%20av%20utmaningar%20inom%20gigekonomin%20i%20sverige.pdf'
            showInShort: null
        -
            title: 'Survey on Gig Economy'
            url: 'https://hhs.qualtrics.com/jfe/form/SV_1SNSZEQJELsfI1v'
            showInShort: null
        -
            title: 'Initiatives and experiments'
            url: 'https://www.giglabsverige.se/initiativ'
            showInShort: '1'
    block_2: "### Are you a gigger, platform owner or in any other way in contact with the gig economy?\nWe would like your help with insights and challenges for our mapping of the gig economy in Sweden. Read more about how you can help on our [website](https://www.giglabsverige.se)."
    contact_email: lisa.hemph@arbetsformedlingen.se
    contact_name: 'Lisa Hemph'
taxonomy:
    category:
        - Cooperation
    status:
        - Ongoing
---

