---
title: 'kontakta oss'
custom:
    menu:
        -
            title: 'samarbeta med oss'
            url: /sv/om-jobtech-development/samarbeta-med-oss
        -
            title: 'inspireras av andra'
            url: /sv/om-jobtech-development/inspireras-av-andra
        -
            title: 'kontakta oss'
            url: /sv/om-jobtech-development/kontakta-oss
    content: "## Hej!\nKul att du vill komma i kontakt med oss. Vi försöker att vara så snabba som möjligt med att svara på alla slags frågor, funderingar eller feedback. Men det underlättar alltid om kontakten kommer rätt väg.\n\nOm du: \n- Vill ha _hjälp eller support_ relaterat till våra produkter så rekommenderar vi att du använder vårt [community](https://forum.jobtechdev.se) så svarar någon av våra utvecklare eller användare så fort som möjligt. Om du inte vill det tar vi även emot frågor [via mail](mailto:jobtechdev@arbetsformedlingen.se).\n\n- Är intresserad av att _jobba hos oss_ så lägger vi upp alla våra annonser [här på siten](https://jobtechdev.se/sv/jobba-hos-oss), men vi tar även emot spontanansökningar [via mail](mailto:recruitment-jobtech@arbetsformedlingen.se).\n\n- Har frågor kring _samarbete_ eller vill bidra med något till ekosystemet kan du alltid maila vår [Community manager](mailto:josefin.berdtson@arbetsformedlingen.se) så hjälper vi till. \n\n- Vill _träffa oss_ så sitter de flesta utspridda i Sverige, men vi har också kontorslokaler i centrala [Stockholm](https://www.google.com/maps/place/Slottsbacken+4,+111+30+Stockholm/@59.3258895,18.0706078,17z/data=!3m1!4b1!4m5!3m4!1s0x465f9d587818d6bb:0x6a3563e7fe09a3bd!8m2!3d59.3258895!4d18.0727965) och Visby där vi kan boka in ett möte.\n\nVi hörs!  \n<BR><BR><BR><BR>"
---

