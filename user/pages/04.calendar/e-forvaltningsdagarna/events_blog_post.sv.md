---
title: 'eForvaltningsdagarna 27-28 oktober 2021'
custom:
    title: eFörvaltningsdagarna
    date: '27-10-2021 09:00'
    short: 'JobTech Development kommer delta på eFörvaltningsdagarna. eFörvaltningsdagarna är Sveriges största konferens och mötesplats om e-förvaltning och digital transformation. Mer information kommer!'
    description: 'JobTech Development kommer delta på eFörvaltningsdagarna. eFörvaltningsdagarna är Sveriges största konferens och mötesplats om e-förvaltning och digital transformation. Mer information kommer!'
    endtime: '18:00'
---

