# jobtechdev-se

This is the code for https://www.jobtechdev.se/ setup. It has a companion repository
[jobtechdev-se-infra](https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-infra)
with code for deployment on OpenShift/Kubernetes.

This repository contains code to build a container image for [Grav CMS](https://getgrav.org/).
Beside Grav, the container contains plugins for Grav CMS, theme and necessary configuration.
Contet of the site is not included, except for an initial site deployed at first run.

The image is intended to be deployed on Kubernetes/OpenShift using the mainfest in
[jobtechdev-se-infra](https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-infra).

When using this setup one cannot do upgrade of Grav, plugins, theme in the Grav user interface.
All those changes are managed by this code and the container. You will get an error if you do
it in the user interface.

Content is edited in the Grav user interface.

Figure below illustrates the deployment setup.

![Deployment image](docs/images/deployment.jpg)


# HowTos

* [Change the JobTech theme](docs/theme.md) - Intended for theme developers. (Swedish)

* [Upgrade to new version of Grav or PHP](docs/upgrade.md) - Intended for administratos and developers comfortable with Grav. (Swedish)

* [Restore backup](https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-infra/-/blob/main/docs/restore.md) - Intended for administrators.

<script> console.log({{ page.header.custom.components|json_encode|raw }}) </script> 
